Este programa obtiene la Transformada de Fourier de un archivo y la imprime
en otro archivo con sufijo _fft.
El archivo debe tener dos columnas, la primera con x y la segunda con f(x).
Puede tener comentarios (con #) o lineas en blanco. Cualquier otro tipo de
lineas conducira a errores o comportamientos inesperados. Todos los valores
se consideran como un solo conjunto de datos.
 
Ejemplo de uso:
    ./fft.py archivo.dat
Eso imprime la Transformada en archivo_fft.dat

Este programa queda en dominio publico. Puede modificarse y relicenciarse.