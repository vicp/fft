#!/usr/bin/env python3

###############################################################################
# Este programa obtiene la Transformada de Fourier de un archivo y la imprime #
# en otro archivo con sufijo _fft.                                            #
# El archivo debe tener dos columnas, la primera con x y la segunda con f(x). #
# Puede tener comentarios (con #) o lineas en blanco. Cualquier otro tipo de  #
# lineas conducira a errores o comportamientos inesperados. Todos los valores #
# se consideran como un solo conjunto de datos reales.                        #
#                                                                             #
# Ejemplo de uso:                                                             #
#     ./fft.py archivo.dat                                                    #
# Eso imprime la Transformada en archivo_fft.dat                              #
#                                                                             #
# Este programa queda en dominio publico. Puede modificarse y relicenciarse.  #
#                                                                             #
# Autor: Victor Hugo Purrello                                    Version: 0.4 #
###############################################################################

import numpy as np
import lzma as xz
import os
import argparse

parser = argparse.ArgumentParser (
        description='Calcula la FFT de un archivo.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument ('file',
        help='archivo de entrada')
parser.add_argument ('-a',
        help='imprime el valor absoluto en vez de la parte real e imaginaria',
        action="store_true")
parser.add_argument ('-v',
        help='comportamiento verbose',
        action="store_true")
args = parser.parse_args()

FULLFILENAME = args.file
FILENAME = os.path.split(FULLFILENAME)[1]
VERBOSE = args.v
ABS = args.a

if VERBOSE:
    print ("Procesando archivo %s" %FILENAME)

# Soporte para archivos comprimidos con xz
if FILENAME[-2:] == "xz":
    fopen = xz.open
else:
    fopen = open

# Manejo de la extension en el nombre del archivo
if FILENAME[-2:] == "xz":
    OUTNAME = FILENAME[:-7]+"_fft"+FILENAME[-7:-3]
elif FILENAME[-4] == '.':
    OUTNAME = FILENAME[:-4]+"_fft"+FILENAME[-4:]
else:
    OUTNAME = FILENAME+"_fft"
if os.path.isfile(OUTNAME):
    print ("%s ya existe" %OUTNAME)
    exit(1)

data = []
with fopen(FULLFILENAME, 'rb') as FILE:
    for line in FILE.readlines():
        # Omito comentarios o lineas en blanco
        if line == b'\n':
            continue
        elif line == b'':
            continue
        elif line.decode()[0] == '#':
            continue
        # Sino tomo los datos como x,f(x)
        else:
            data.append(line.split()[0:2])

data = np.array(data, dtype=float)

# Verificacion de que el archivo este ordenado, sino falla el step
if not all(data[:,0] == sorted(data[:,0])):
    print ("No esta soportado trabajar con archivos desordenados")
    exit(1)

fy = np.fft.rfft(data[:,1])

if data[1,0]-data[0,0] != 0:
    steps = data[1,0]-data[0,0]
else:
    print("Cuidado: El archivo tiene los valores de x repetidos.")
    steps = (data[2,0]-data[0,0])/2.0

fx = np.fft.rfftfreq(data[:,0].shape[-1], d=steps)

with open(OUTNAME,'w') as OUTFILE:
    if not ABS:
        OUTFILE.write("# fx fy.real fy.imag\n")
    for j in range(len(fx)):
        if ABS:
            OUTFILE.write("%g %g\n" %(fx[j], np.sqrt(fy.real[j]**2 + fy.imag[j]**2)))
        else:
            OUTFILE.write("%e %e %e\n" %(fx[j], fy.real[j], fy.imag[j]))
